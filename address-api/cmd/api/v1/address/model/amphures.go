package model

import "gorm.io/gorm"

//Amphure is a model for amphure
type Amphure struct {
	gorm.Model `json:"-"`
	ID         uint   `json:"id"`
	Code       string `json:"code"`
	NameTh     string `json:"nameTh"`
	NameEn     string `json:"nameEn"`
	ProvinceID int    `json:"provinceId"`
}
