package model

import "gorm.io/gorm"

//District is a model for district
type District struct {
	gorm.Model `json:"-"`
	ID         uint   `json:"id"`
	ZipCode    int    `json:"zipCode"`
	NameTh     string `json:"nameTh"`
	NameEn     string `json:"nameEn"`
	AmphureID  int    `json:"amphureId"`
}
