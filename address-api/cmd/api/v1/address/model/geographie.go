package model

import "gorm.io/gorm"

//Geographie is a model for geographie
type Geographie struct {
	gorm.Model
	Name string `json:"name"`
}
