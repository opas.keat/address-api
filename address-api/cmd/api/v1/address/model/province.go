package model

import "gorm.io/gorm"

//Province is a model for province
type Province struct {
	gorm.Model  `json:"-"`
	ID          uint   `json:"id"`
	Code        string `json:"code"`
	NameTh      string `json:"nameTh"`
	NameEn      string `json:"nameEn"`
	GeographyID int    `json:"geographyID"`
}
