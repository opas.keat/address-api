package api

import (
	"bytes"
	"efilingsftp/cmd/v1/database"
	"efilingsftp/cmd/v1/models"
	"efilingsftp/cmd/v1/util"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/compress"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/proxy"
	"github.com/spf13/viper"
)

func Login(c *fiber.Ctx) error {
	fmt.Println("Login API...")
	viper.SetConfigName("env")
	viper.AddConfigPath(".")
	viper.AutomaticEnv()
	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("fatal error config file: %s", err))
	}
	type Auth struct {
		UserName string `json:"user_name"`
		UserPass string `json:"user_pass"`
	}
	auth := new(Auth)
	if err := c.BodyParser(auth); err != nil {
		panic("An error occurred when parsing the user: " + err.Error())
	}
	// data := url.Values{
	// 	"name":       {""},
	// 	"passwords": {"gardener"},
	// }
	values := map[string]string{"name": auth.UserName, "passwords": auth.UserPass}
	jsonValue, _ := json.Marshal(values)
	DmsApiServer := viper.GetString("app.dmsapi")
	// DmsApiServer = "http://161.82.233.58/pxapi-cloud/"
	response, err2 := http.Post(DmsApiServer+"/pxapi/api/v1/users/login", "application/json", bytes.NewBuffer(jsonValue))
	if err2 != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err2)
	}
	defer response.Body.Close()
	bodyBytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Print(err.Error())
	}
	type LoginResponse struct {
		ErrorMessage string `json:"errorMessage"`
		Message      string `json:"message"`
	}
	var responseObject LoginResponse
	json.Unmarshal(bodyBytes, &responseObject)
	fmt.Printf("Login as struct %+v\n", responseObject)

	return c.JSON(responseObject)
}

func ListFileAttachByDateRange(db *database.Database) fiber.Handler {
	return func(c *fiber.Ctx) error {
		fmt.Println("ListFileAttachByDateRange API...")

		viper.SetConfigName("env")
		viper.AddConfigPath(".")
		viper.AutomaticEnv()
		err := viper.ReadInConfig()

		if err != nil {
			panic(fmt.Errorf("fatal error config file: %s", err))
		}

		fmt.Println("From IP : " + c.IP())
		allowlist := viper.GetString("app.allowlist")
		if !strings.Contains(allowlist, c.IP()) {
			return c.JSON("Authorize Fail From This IP.")
		}

		DmsApiServer := viper.GetString("app.dmsapi")
		// offset := c.Query("offset", "0")
		// limit := c.Query("limit", "20")
		dateFrom := c.Query("dateFrom", "0")
		dateTo := c.Query("dateTo", "0")
		docType := c.Query("docType", "0")
		sysId := c.Query("sysId", "2")
		if sysId == "2" {
			sysId = "0"
		}
		// fmt.Println("dateFrom = " + dateFrom)
		// fmt.Println("dateTo = " + dateTo)
		// fmt.Println("docType = " + docType)
		logs := models.Logs{
			ID:        0,
			CreatedBy: sysId,
			Ip:        c.IP(),
			UseApi:    "viewbydate",
		}
		if response := db.Create(&logs); response.Error != nil {
			panic("Error occurred while create logs from the database: " + response.Error.Error())
		}
		dateApproveFrom := util.ConvertDateToThai(dateFrom, "-")
		dateApprovTo := util.ConvertDateToThai(dateTo, "-")
		values := map[string]string{"dateForm": "", "dateTo": "", "dateApproveFrom": dateApproveFrom, "dateApprovTo": dateApprovTo, "attachCode": docType, "sysId": sysId}
		jsonValue, _ := json.Marshal(values)
		response, err2 := http.Post(DmsApiServer+"/pxapi/api/v1/fileAttachs/getAttachCios?version=1&api_key=praXis", "application/json", bytes.NewBuffer(jsonValue))
		if err2 != nil {
			fmt.Printf("The HTTP request failed with error %s\n", err2)
		}
		defer response.Body.Close()
		bodyBytes, err := ioutil.ReadAll(response.Body)
		if err != nil {
			fmt.Print(err.Error())
		}
		var responseObject models.CiosResponse
		json.Unmarshal(bodyBytes, &responseObject)
		fmt.Print(responseObject)
		var docList []models.EFiling
		for i, cios := range responseObject.Data {
			fmt.Println(i, cios.Dmsdocument.ID)
			var fileLists []models.FileList
			for _, at := range cios.Listaattach {
				fileLists = append(fileLists, models.FileList{
					RefNo:         strconv.Itoa(at.ID),
					DocTypeID:     at.Type,
					Issue:         int(at.Order),
					FileType:      at.Fileattachtype[1:],
					FileName:      at.Fileattachname[0:strings.LastIndex(at.Fileattachname, ".")],
					FileSize:      at.Fileattachsize,
					Pages:         at.Amountpages,
					OwnerType:     at.Ownertype,
					OwnerName:     at.Ownername,
					InputFileType: at.Inputfiletype,
					AttachDate:    at.Createddate,
					ApproveDate:   at.ApproveDate,
					ApproveName:   at.Approvename,
				})
			}
			acDate := strings.Split(cios.Dmsdocument.Documentdate01, " ")
			docList = append(docList, models.EFiling{
				SystemID:   1,  //ส่งเพิ่ม
				SystemName: "", //ส่งเพิ่ม
				CourtCode:  "", //ส่งเพิ่ม
				CourtName:  "", //ส่งเพิ่ม
				AcceptDate: acDate[0],
				BlackNo:    cios.Dmsdocument.Documentname,
				Charge:     cios.Dmsdocument.Documenttext02,
				FolderID:   strconv.Itoa(cios.Dmsdocument.Documentfolderid),
				Plaintiff:  cios.Dmsdocument.Documentvarchar01,
				Defendant:  cios.Dmsdocument.Documentvarchar02,
				CaseType:   cios.Dmsdocument.Documentvarchar04,
				RedNo:      cios.Dmsdocument.Documentvarchar06,
				FileList:   fileLists,
			})
		}

		return c.JSON(docList)
	}
}

func ListFileAttachByDocName(db *database.Database) fiber.Handler {
	return func(c *fiber.Ctx) error {
		fmt.Println("ListFileAttachByDocName API...")
		viper.SetConfigName("env")
		viper.AddConfigPath(".")
		viper.AutomaticEnv()
		err := viper.ReadInConfig()

		if err != nil {
			panic(fmt.Errorf("fatal error config file: %s", err))
		}

		fmt.Println("From IP : " + c.IP())
		allowlist := viper.GetString("app.allowlist")
		if !strings.Contains(allowlist, c.IP()) {
			return c.JSON("Authorize Fail From This IP.")
		}

		DmsApiServer := viper.GetString("app.dmsapi")
		offset := c.Query("offset", "0")
		limit := c.Query("limit", "20")
		caseNumber := c.Query("caseNumber", "0")
		docType := c.Query("docType", "0")
		sysId := c.Query("sysId")
		if sysId == "2" {
			sysId = "0"
		}

		logs := models.Logs{
			ID:        0,
			CreatedBy: sysId,
			Ip:        c.IP(),
			UseApi:    "viewAll",
		}
		if response := db.Create(&logs); response.Error != nil {
			panic("Error occurred while create logs from the database: " + response.Error.Error())
		}

		params := url.Values{}
		params.Add("version", "1")
		params.Add("offset", offset)
		params.Add("limit", limit)
		params.Add("sort", "createdDate")
		params.Add("dir", "asc")
		params.Add("sysId", sysId)
		params.Add("caseNumber", caseNumber)
		params.Add("docType", docType)
		params.Add("api_key", "praXis")
		response, err := http.Get(DmsApiServer + "/pxapi/api/v1/dmsDocuments/getAttachByDocNameConnect?" + params.Encode())
		if err != nil {
			fmt.Printf("The HTTP request failed with error %s\n", err)
		}
		defer response.Body.Close()
		bodyBytes, err := ioutil.ReadAll(response.Body)
		if err != nil {
			fmt.Print(err.Error())
		}
		var responseObject models.SearchResponse
		json.Unmarshal(bodyBytes, &responseObject)
		fmt.Printf("ListFileAttachByDocName SearchResponse as struct %+v\n", responseObject)
		acDate := strings.Split(responseObject.DataDoc.DocumentDate01, " ")
		efiling := models.EFiling{
			SystemID:   responseObject.DataDoc.SysID,
			SystemName: responseObject.DataDoc.SysName,
			RefNo:      responseObject.DataDoc.RefNo,
			CourtCode:  responseObject.DataDoc.CourtCode,
			CourtName:  responseObject.DataDoc.CourtName,
			AcceptDate: acDate[0],
			BlackNo:    responseObject.DataDoc.DocumentName,
			Charge:     responseObject.DataDoc.DocumentText02,
			FolderID:   strconv.Itoa(responseObject.DataDoc.DocumentFolderID),
			Plaintiff:  responseObject.DataDoc.DocumentVarchar01,
			Defendant:  responseObject.DataDoc.DocumentVarchar02,
			CaseType:   responseObject.DataDoc.DocumentVarchar04,
			RedNo:      responseObject.DataDoc.DocumentVarchar06,

			FileList: []models.FileList{},
		}
		var fileLists []models.FileList
		for _, dt := range responseObject.Data {
			fileLists = append(fileLists, models.FileList{
				RefNo:         strconv.Itoa(dt.ID),
				DocTypeID:     dt.Type,
				Issue:         int(dt.Order),
				FileType:      dt.FileAttachType[1:],
				FileName:      dt.FileAttachName[0:strings.LastIndex(dt.FileAttachName, ".")],
				FileSize:      dt.FileAttachSize,
				Pages:         dt.Pages,
				ApproveName:   dt.ApproveName,
				OwnerType:     dt.OwnerType,
				OwnerName:     dt.OwnerName,
				InputFileType: dt.InputFileType,
				AttachDate:    dt.CreatedDate,
				ApproveDate:   dt.ApproveDate,
				EfMapDocID:    dt.EfMapDocID,
			})
		}
		efiling.FileList = fileLists
		// return efiling
		return c.JSON(efiling)
	}
}

func GetFileAttachById(c *fiber.Ctx) error {
	fmt.Println("GetFileAttachById API...")
	viper.SetConfigName("env")
	viper.AddConfigPath(".")
	viper.AutomaticEnv()
	err := viper.ReadInConfig()

	if err != nil {
		panic(fmt.Errorf("fatal error config file: %s", err))
	}

	fmt.Println("From IP : " + c.IP())
	allowlist := viper.GetString("app.allowlist")
	if !strings.Contains(allowlist, c.IP()) {
		return c.JSON("Authorize Fail From This IP.")
	}

	DmsApiServer := viper.GetString("app.dmsapi")
	fileAttachId := c.Query("fileAttachId", "0")
	// fileAttachId = "553"
	params := url.Values{}
	params.Add("version", "1")
	params.Add("api_key", "praXis")
	response, err := http.Get(DmsApiServer + "/pxapi/api/v1/fileAttachs/" + fileAttachId + "?" + params.Encode())
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
	}
	defer response.Body.Close()
	bodyBytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Print(err.Error())
	}
	var responseObject models.FileAttachResponse
	json.Unmarshal(bodyBytes, &responseObject)
	fmt.Printf("GetFileAttachById FileAttachResponse as struct %+v\n", responseObject)
	return c.JSON(responseObject.Data.URL)
}

func DownloadFileAttach(c *fiber.Ctx) error {
	fmt.Println("DownloadFileAttach API...")
	viper.SetConfigName("env")
	viper.AddConfigPath(".")
	viper.AutomaticEnv()
	err := viper.ReadInConfig()

	if err != nil {
		panic(fmt.Errorf("fatal error config file: %s", err))
	}

	fmt.Println("From IP : " + c.IP())
	allowlist := viper.GetString("app.allowlist")
	if !strings.Contains(allowlist, c.IP()) {
		return c.JSON("Authorize Fail From This IP.")
	}
	DmsApiServer := viper.GetString("app.dmsapi")
	fileAttachId := c.Params("fileAttachId", "0")
	// fileAttachId = "553"
	params := url.Values{}
	params.Add("version", "1")
	params.Add("api_key", "praXis")
	// response, err := http.Get(DmsApiServer + "/pxapi/api/v1/fileAttachs/" + strconv.Itoa(fileAttachId) + "/dl?" + params.Encode())
	// if err != nil {
	// 	fmt.Printf("The HTTP request failed with error %s\n", err)
	// }
	// defer response.Body.Close()

	// // Create the file
	// out, err := os.Create(filePath)
	// if err != nil {
	// 	return err
	// }
	// defer out.Close()

	// // Write the body to file
	// _, err = io.Copy(out, response.Body)
	url := DmsApiServer + "/pxapi/api/v1/fileAttachs/" + fileAttachId + "/dl?" + params.Encode()
	fmt.Println("url = " + url)
	if err := proxy.Do(c, url); err != nil {
		return err
	}
	// Remove Server header from response
	c.Response()
	return nil
	// return c.Download(DmsApiServer + "/pxapi/api/v1/fileAttachs/" + fileAttachId + "/dl?" + params.Encode())
	// return err
}

func DownloadFileAttach2(c *fiber.Ctx) error {
	fmt.Println("DownloadFileAttach2 API...")
	viper.SetConfigName("env")
	viper.AddConfigPath(".")
	viper.AutomaticEnv()
	err := viper.ReadInConfig()

	if err != nil {
		panic(fmt.Errorf("fatal error config file: %s", err))
	}

	dbPraxticol := viper.GetString("app.dbpraxticol")
	fileAttachId := c.Query("fileAttachId", "0")
	fileName := c.Query("fileName", "0")
	fmt.Println("dbPraxticol = " + dbPraxticol)
	fmt.Println("fileAttachId = " + fileAttachId)
	fmt.Println("fileName = " + fileName)
	return c.Download(dbPraxticol+fileAttachId, fileName)
}

func ReportCountFileAttach(c *fiber.Ctx) error {
	fmt.Println("ReportCountFileAttach API...")
	viper.SetConfigName("env")
	viper.AddConfigPath(".")
	viper.AutomaticEnv()
	err := viper.ReadInConfig()

	DmsApiServer := viper.GetString("app.dmsapi")

	layout := "2006-01-02"
	dateForm := c.Query("dateForm", "")
	dateFormStr := ""
	if dateForm != "" {
		t, _ := time.Parse(layout, dateForm)
		buddhaDate := t.AddDate(543, 0, 0)
		dateFormStr = buddhaDate.Format("02/01/2006")
	}

	dateTo := c.Query("dateTo", "")
	dateToStr := ""
	if dateTo != "" {
		t, _ := time.Parse(layout, dateTo)
		buddhaDate := t.AddDate(543, 0, 0)
		dateToStr = buddhaDate.Format("02/01/2006")
	}

	dateForm2 := c.Query("dateForm2", "")
	dateForm2Str := ""
	if dateForm2 != "" {
		dateForm2Int, _ := strconv.Atoi(dateForm2)
		// println(dateForm2Int)
		dateForm2Str = strconv.Itoa(dateForm2Int + 543)
	}
	dateTo2 := c.Query("dateTo2", "")
	dateTo2Str := ""
	if dateTo2 != "" {
		dateTo2Int, _ := strconv.Atoi(dateTo2)
		dateTo2Str = strconv.Itoa(dateTo2Int + 543)
	}

	folderId := c.Query("folderId", "0")
	folderId2, _ := strconv.Atoi(folderId)
	// println(dateFormStr)
	// println(dateToStr)
	// println(dateForm2Str)
	// println(dateTo2Str)
	// println(folderId2)
	q := models.ReportCountFileAttachPost{
		DateForm:  dateFormStr,
		DateTo:    dateToStr,
		DateForm2: dateForm2Str,
		DateTo2:   dateTo2Str,
		FolderID:  folderId2,
	}
	json_data, err := json.Marshal(q)
	if err != nil {
		fmt.Println(err)
		// return responseId
	}
	response, err := http.Post(DmsApiServer+"/pxapi/api/v1/dmsFolder/coj350ReportCountAttachAll?version=1&api_key=praXis", "application/json", bytes.NewBuffer(json_data))
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
	}
	defer response.Body.Close()
	bodyBytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Print(err.Error())
	}
	var responseObject models.ReportCountFileAttachResponse
	json.Unmarshal(bodyBytes, &responseObject)
	fmt.Printf("ReportCountFileAttach as struct %+v\n", responseObject)
	reportCount := models.ReportCountFileAttach{
		TotalDoc:      responseObject.TotalDoc,
		HaveAttach:    responseObject.HaveAttach,
		NotHaveAttach: responseObject.NotHaveAttach,
	}
	return c.JSON(reportCount)
}

func API(db *database.Database) {
	viper.SetConfigName("env")
	viper.AddConfigPath(".")
	viper.AutomaticEnv()
	err := viper.ReadInConfig()

	if err != nil {
		panic(fmt.Errorf("fatal error config file: %s", err))
	}
	port := viper.GetString("app.port")

	app := fiber.New()
	app.Use(cors.New())
	// app.Use(csrf.New())
	app.Use(compress.New(compress.Config{
		Level: compress.LevelBestSpeed, // 1
	}))
	app.Use(logger.New(logger.Config{
		Format:     "${cyan}[${time}] ${white}${pid} ${red}${status} ${blue}[${method}] ${white}${path}\n",
		TimeFormat: "02-Jan-2006",
		TimeZone:   "Asia/Jakarta",
	}))

	app.Get("/", func(c *fiber.Ctx) error {
		return c.JSON(fiber.Map{
			"message": "🐣 DMS API.",
		})
	})

	api := app.Group("/api")
	v1 := api.Group("/v1", func(c *fiber.Ctx) error {
		c.JSON(fiber.Map{
			"message": "🐣 v1",
		})
		return c.Next()
	})

	v1.Post("/login", Login)
	v1.Get("/viewall", ListFileAttachByDocName(db))
	v1.Get("/viewbydate", ListFileAttachByDateRange(db))
	v1.Get("/viewfile/:fileAttachId", GetFileAttachById)
	v1.Get("/download/:fileAttachId", DownloadFileAttach)
	v1.Get("/dl", DownloadFileAttach2)
	v1.Get("/reportcountfileattach", ReportCountFileAttach)
	// proxy.Forward("https://i.imgur.com/IWaBepg.gif")
	// app.Get("/download", proxy.Forward(DmsApiServer + "/pxapi/api/v1/fileAttachs/" + strconv.Itoa(fileAttachId) + "/dl?" + params.Encode()))

	err = app.Listen(port)
	if err != nil {
		panic(err)
	}
}
