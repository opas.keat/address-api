package database

import (
	"strconv"
	"strings"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"gorm.io/gorm/schema"
)

type DatabaseConfig struct {
	Driver   string
	Host     string
	Username string
	Password string
	Port     int
	Database string
}

type Database struct {
	*gorm.DB
}

func New(config *DatabaseConfig) (*Database, error) {
	var db *gorm.DB
	var err error
	switch strings.ToLower(config.Driver) {
	case "mysql":
		dsn := config.Username + ":" + config.Password + "@tcp(" + config.Host + ":" + strconv.Itoa(config.Port) + ")/" + config.Database + "?charset=utf8mb4&collation=utf8mb4_general_ci&parseTime=True&loc=UTC"
		db, err = gorm.Open(mysql.Open(dsn), &gorm.Config{
			PrepareStmt:     true,
			CreateBatchSize: 100,
			NamingStrategy: schema.NamingStrategy{
				TablePrefix:   "coj_link_",                       // table name prefix, table for `User` would be `t_users`
				SingularTable: true,                              // use singular table name, table for `User` would be `user` with this option enabled
				NameReplacer:  strings.NewReplacer("CID", "Cid"), // use name replacer to change struct/field name before convert it to db name
			},
			Logger: logger.Default.LogMode(logger.Warn),
		})
		// 	break
		// case "postgresql", "postgres":
		// 	dsn := "user=" + config.Username + " password=" + config.Password + " dbname=" + config.Database + " host=" + config.Host + " port=" + strconv.Itoa(config.Port) + " TimeZone=UTC"
		// 	db, err = gorm.Open(postgres.Open(dsn), &gorm.Config{PrepareStmt:     true,
		// 		CreateBatchSize: 100,
		// 		NamingStrategy: schema.NamingStrategy{
		// 			TablePrefix:   "ect_",                            // table name prefix, table for `User` would be `t_users`
		// 			SingularTable: true,                              // use singular table name, table for `User` would be `user` with this option enabled
		// 			NameReplacer:  strings.NewReplacer("CID", "Cid"), // use name replacer to change struct/field name before convert it to db name
		// 		},
		// 		Logger: logger.Default.LogMode(logger.Warn),})
		// case "sqlserver", "mssql":
		// 	dsn := "sqlserver://" + config.Username + ":" + config.Password + "@" + config.Host + ":" + strconv.Itoa(config.Port) + "?database=" + config.Database
		// 	db, err = gorm.Open(sqlserver.Open(dsn), &gorm.Config{})
		// 	break
	}
	return &Database{db}, err
}
