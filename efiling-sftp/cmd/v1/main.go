package main

import (
	"efilingsftp/cmd/v1/api"
	"efilingsftp/cmd/v1/database"
	"efilingsftp/cmd/v1/models"
	"fmt"

	"github.com/spf13/viper"
)

type App struct {
	// *fiber.App

	DB *database.Database
	// Session *session.Session
	// FilePath *string
}

func main() {
	fmt.Println("Starting the application...COJ LINK Version 1.0.0")
	viper.SetConfigName("env")
	viper.AddConfigPath(".")
	viper.AutomaticEnv()
	err := viper.ReadInConfig()

	if err != nil {
		panic(fmt.Errorf("fatal error config file: %s", err))
	}

	app := App{
		// App: fiber.New(*config.GetFiberConfig()),
		// Hasher:  hashing.New(config.GetHasherConfig()),
		// Session: session.New(config.GetSessionConfig()),
	}
	// sftpPath := viper.GetString("app.sftppath")
	// tempPath := viper.GetString("app.temppath")
	// unzippath := viper.GetString("app.unzippath")
	// encrypt := viper.GetString("app.encrypt")
	// DmsApiServer := viper.GetString("app.dmsapi")
	// sftp.GetEFiling(DmsApiServer, sftpPath, tempPath, unzippath, encrypt)
	// sftp.GetUserProfileIdByFullName("https://ab6fdd434e1c.ngrok.io", "นายแพทย์ชนิดศาลาชาโดยมีชื่อยาวนะยาชนิดศาลาชาโดยมีชื่อยาวนะ ณ ตำแหน่งหลากหลายณ ตำแหน่งหลากหลายณ")

	// Initialize database
	fmt.Println("Initialize database.")
	db, err := database.New(&database.DatabaseConfig{
		Driver:   viper.GetString("app.dbdriver"),
		Host:     viper.GetString("app.dbserver"),
		Username: viper.GetString("app.dbuser"),
		Password: viper.GetString("app.dbpass"),
		Port:     3306,
		Database: viper.GetString("app.dbname"),
	})

	// // Auto-migrate database models
	if err != nil {
		fmt.Println("failed to connect to database:", err.Error())
	} else {
		if db == nil {
			fmt.Println("failed to connect to database: db variable is nil")
		} else {
			app.DB = db
			err := app.DB.AutoMigrate(&models.Logs{})
			if err != nil {
				fmt.Println("failed to automigrate Log model:", err.Error())
				return
			}
		}
	}

	api.API(db)
	//GOOS=windows GOARCH=amd64 go build -o bin/efilingsftp.exe cmd/v1/main.go
	//GOOS=windows GOARCH=amd64 go build -o bin/cojLink.exe cmd/v1/main.go
}
