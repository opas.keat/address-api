package models

type DocumentSave struct {
	AcceptDate string `json:"documentDate01"`
	BlackNo    string `json:"documentName"`
	Charge     string `json:"documentText02"`
	FolderId   int    `json:"documentFolderId"`
	Plaintiff  string `json:"documentVarchar01"`
	Defendant  string `json:"documentVarchar02"`
	CaseType   string `json:"documentVarchar04"`
	RedNo      string `json:"documentVarchar06"`
}

type DocumentResponse struct {
	Data         DocumentResponseData `json:"data"`
	Success      bool                 `json:"success"`
	ErrorMessage string               `json:"errorMessage"`
	Message      string               `json:"message"`
}

type DocumentResponseStructure struct {
	ID            int         `json:"id"`
	OrderNo       float64     `json:"orderNo"`
	Name          string      `json:"name"`
	ShortName     interface{} `json:"shortName"`
	Detail        interface{} `json:"detail"`
	Code          interface{} `json:"code"`
	NodeLevel     int         `json:"nodeLevel"`
	ParentID      int         `json:"parentId"`
	ParentKey     string      `json:"parentKey"`
	StructureTree string      `json:"structureTree"`
}

type DocumentResponseTitle struct {
	ID          int         `json:"id"`
	Name        string      `json:"name"`
	NameEng     string      `json:"nameEng"`
	CreatedDate string      `json:"createdDate"`
	Type        interface{} `json:"type"`
}

type DocumentResponseUserProfileType struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type DocumentResponseUser struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type DocumentResponseUserStatus struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type DocumentResponseUserProfileCreate struct {
	ID              int                             `json:"id"`
	OrderNo         float64                         `json:"orderNo"`
	Structure       DocumentResponseStructure       `json:"structure"`
	Title           DocumentResponseTitle           `json:"title"`
	FirstName       string                          `json:"firstName"`
	LastName        string                          `json:"lastName"`
	FullName        string                          `json:"fullName"`
	Email           string                          `json:"email"`
	UserProfileType DocumentResponseUserProfileType `json:"userProfileType"`
	Tel             string                          `json:"tel"`
	FirstNameEng    string                          `json:"firstNameEng"`
	LastNameEng     string                          `json:"lastNameEng"`
	FullNameEng     string                          `json:"fullNameEng"`
	Code            string                          `json:"code"`
	Address         string                          `json:"address"`
	Position        interface{}                     `json:"position"`
	PositionType    interface{}                     `json:"positionType"`
	PositionLevel   int                             `json:"positionLevel"`
	User            DocumentResponseUser            `json:"user"`
	UserStatus      DocumentResponseUserStatus      `json:"userStatus"`
	CojEmpCode      string                          `json:"cojEmpCode"`
}

type DocumentResponseStructure0 struct {
	ID            int         `json:"id"`
	OrderNo       float64     `json:"orderNo"`
	Name          string      `json:"name"`
	ShortName     interface{} `json:"shortName"`
	Detail        interface{} `json:"detail"`
	Code          interface{} `json:"code"`
	NodeLevel     int         `json:"nodeLevel"`
	ParentID      int         `json:"parentId"`
	ParentKey     string      `json:"parentKey"`
	StructureTree string      `json:"structureTree"`
}

type DocumentResponseTitle0 struct {
	ID          int         `json:"id"`
	Name        string      `json:"name"`
	NameEng     string      `json:"nameEng"`
	CreatedDate string      `json:"createdDate"`
	Type        interface{} `json:"type"`
}

type DocumentResponseUserProfileType0 struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type DocumentResponseUser0 struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type DocumentResponseUserStatus0 struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type DocumentResponseUserProfileUpdate struct {
	ID              int                              `json:"id"`
	OrderNo         float64                          `json:"orderNo"`
	Structure       DocumentResponseStructure0       `json:"structure"`
	Title           DocumentResponseTitle0           `json:"title"`
	FirstName       string                           `json:"firstName"`
	LastName        string                           `json:"lastName"`
	FullName        string                           `json:"fullName"`
	Email           string                           `json:"email"`
	UserProfileType DocumentResponseUserProfileType0 `json:"userProfileType"`
	Tel             string                           `json:"tel"`
	FirstNameEng    string                           `json:"firstNameEng"`
	LastNameEng     string                           `json:"lastNameEng"`
	FullNameEng     string                           `json:"fullNameEng"`
	Code            string                           `json:"code"`
	Address         string                           `json:"address"`
	Position        interface{}                      `json:"position"`
	PositionType    interface{}                      `json:"positionType"`
	PositionLevel   int                              `json:"positionLevel"`
	User            DocumentResponseUser0            `json:"user"`
	UserStatus      DocumentResponseUserStatus0      `json:"userStatus"`
	CojEmpCode      string                           `json:"cojEmpCode"`
}

type DocumentResponseData struct {
	ID                       int                               `json:"id"`
	CreatedDate              string                            `json:"createdDate"`
	CreatedBy                int                               `json:"createdBy"`
	RemovedDate              string                            `json:"removedDate"`
	RemovedBy                int                               `json:"removedBy"`
	UpdatedDate              string                            `json:"updatedDate"`
	UpdatedBy                int                               `json:"updatedBy"`
	DocumentPublicDate       string                            `json:"documentPublicDate"`
	DocumentExpireDate       string                            `json:"documentExpireDate"`
	DmsDocumentPreExpireDate interface{}                       `json:"dmsDocumentPreExpireDate"`
	DocumentDate01           string                            `json:"documentDate01"`
	DocumentDate02           string                            `json:"documentDate02"`
	DocumentDate03           string                            `json:"documentDate03"`
	DocumentDate04           string                            `json:"documentDate04"`
	DocumentDate05           string                            `json:"documentDate05"`
	DocumentDate06           string                            `json:"documentDate06"`
	DocumentDate07           string                            `json:"documentDate07"`
	DocumentDate08           string                            `json:"documentDate08"`
	DocumentDate09           string                            `json:"documentDate09"`
	DocumentDate10           string                            `json:"documentDate10"`
	DocumentDate11           string                            `json:"documentDate11"`
	DocumentDate12           string                            `json:"documentDate12"`
	DocumentDate13           string                            `json:"documentDate13"`
	DocumentDate14           string                            `json:"documentDate14"`
	DocumentDate15           string                            `json:"documentDate15"`
	DocumentDate16           string                            `json:"documentDate16"`
	DocumentDate17           string                            `json:"documentDate17"`
	DocumentDate18           string                            `json:"documentDate18"`
	DocumentDate19           string                            `json:"documentDate19"`
	DocumentDate20           string                            `json:"documentDate20"`
	DocumentDate21           string                            `json:"documentDate21"`
	DocumentDate22           string                            `json:"documentDate22"`
	DocumentDate23           string                            `json:"documentDate23"`
	DocumentDate24           string                            `json:"documentDate24"`
	DocumentDate25           string                            `json:"documentDate25"`
	DocumentDate26           string                            `json:"documentDate26"`
	DocumentDate27           string                            `json:"documentDate27"`
	DocumentDate28           string                            `json:"documentDate28"`
	DocumentDate29           string                            `json:"documentDate29"`
	DocumentDate30           string                            `json:"documentDate30"`
	DocumentDate31           string                            `json:"documentDate31"`
	DocumentDate32           string                            `json:"documentDate32"`
	DocumentDate33           string                            `json:"documentDate33"`
	DocumentDate34           string                            `json:"documentDate34"`
	DocumentDate35           string                            `json:"documentDate35"`
	DocumentDate36           string                            `json:"documentDate36"`
	DocumentDate37           string                            `json:"documentDate37"`
	DocumentDate38           string                            `json:"documentDate38"`
	DocumentDate39           string                            `json:"documentDate39"`
	DocumentDate40           string                            `json:"documentDate40"`
	DocumentTypeID           int                               `json:"documentTypeId"`
	DocumentName             string                            `json:"documentName"`
	DocumentPublicStatus     interface{}                       `json:"documentPublicStatus"`
	DocumentFolderID         int                               `json:"documentFolderId"`
	DocumentFloat01          float64                           `json:"documentFloat01"`
	DocumentFloat02          float64                           `json:"documentFloat02"`
	DocumentVarchar01        string                            `json:"documentVarchar01"`
	DocumentVarchar02        string                            `json:"documentVarchar02"`
	DocumentVarchar03        interface{}                       `json:"documentVarchar03"`
	DocumentVarchar04        string                            `json:"documentVarchar04"`
	DocumentVarchar05        interface{}                       `json:"documentVarchar05"`
	DocumentVarchar06        string                            `json:"documentVarchar06"`
	DocumentVarchar07        interface{}                       `json:"documentVarchar07"`
	DocumentVarchar08        string                            `json:"documentVarchar08"`
	DocumentVarchar09        interface{}                       `json:"documentVarchar09"`
	DocumentVarchar10        interface{}                       `json:"documentVarchar10"`
	DocumentVarchar11        interface{}                       `json:"documentVarchar11"`
	DocumentVarchar12        interface{}                       `json:"documentVarchar12"`
	DocumentVarchar13        interface{}                       `json:"documentVarchar13"`
	DocumentVarchar14        interface{}                       `json:"documentVarchar14"`
	DocumentVarchar15        interface{}                       `json:"documentVarchar15"`
	DocumentText01           interface{}                       `json:"documentText01"`
	DocumentText02           string                            `json:"documentText02"`
	DocumentText03           interface{}                       `json:"documentText03"`
	DocumentText04           interface{}                       `json:"documentText04"`
	DocumentText05           interface{}                       `json:"documentText05"`
	DocumentText06           interface{}                       `json:"documentText06"`
	DocumentText07           interface{}                       `json:"documentText07"`
	DocumentText08           interface{}                       `json:"documentText08"`
	DocumentText09           interface{}                       `json:"documentText09"`
	DocumentText10           interface{}                       `json:"documentText10"`
	DocumentText11           interface{}                       `json:"documentText11"`
	DocumentText12           interface{}                       `json:"documentText12"`
	DocumentText13           interface{}                       `json:"documentText13"`
	DocumentText14           interface{}                       `json:"documentText14"`
	DocumentText15           interface{}                       `json:"documentText15"`
	DocumentInt01            int                               `json:"documentInt01"`
	DocumentInt02            int                               `json:"documentInt02"`
	DocumentInt03            int                               `json:"documentInt03"`
	DocumentInt04            int                               `json:"documentInt04"`
	DocumentInt05            int                               `json:"documentInt05"`
	DmsDocumentSec           int                               `json:"dmsDocumentSec"`
	ExpType                  interface{}                       `json:"expType"`
	ExpNumber                int                               `json:"expNumber"`
	UserProfileCreate        DocumentResponseUserProfileCreate `json:"userProfileCreate"`
	UserProfileUpdate        DocumentResponseUserProfileUpdate `json:"userProfileUpdate"`
	UserProfileDel           interface{}                       `json:"userProfileDel"`
	IsExp                    string                            `json:"isExp"`
	DmsSearchID              interface{}                       `json:"dmsSearchId"`
	FullPathName             string                            `json:"fullPathName"`
	BorrowStatus             int                               `json:"borrowStatus"`
	CheckInOut               int                               `json:"checkInOut"`
	CojID                    interface{}                       `json:"cojId"`
	DocRef                   interface{}                       `json:"docRef"`
	JudgeOwner               string                            `json:"judgeOwner"`
}
