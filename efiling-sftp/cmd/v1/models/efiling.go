package models

type EFiling struct {
	SystemID   int        `json:"system_id"`
	SystemName string     `json:"system_name"`
	RefNo      string     `json:"ref_no"`
	CourtCode  string     `json:"court_code"`
	CourtName  string     `json:"court_name"`
	FolderID   string     `json:"folder_id"`
	BlackNo    string     `json:"black_no"`
	RedNo      string     `json:"red_no"`
	Plaintiff  string     `json:"plaintiff"`
	Defendant  string     `json:"defendant"`
	Charge     string     `json:"charge"`
	AcceptDate string     `json:"accept_date"`
	CaseType   string     `json:"case_type"`
	FileList   []FileList `json:"file_list"`
}

type FileList struct {
	RefNo         string `json:"ref_no"`
	DocTypeID     int    `json:"doc_type_id"`
	Issue         int    `json:"issue"`
	FileType      string `json:"file_type"`
	FileName      string `json:"file_name"`
	FileSize      int    `json:"file_size"`
	Pages         int    `json:"page"`
	ApproveName   string `json:"approve_name"`
	OwnerType     string `json:"owner_type"`
	OwnerName     string `json:"owner_name"`
	InputFileType string `json:"input_file_type"`
	AttachDate    string `json:"attach_date"`
	ApproveDate   string `json:"approve_date"`
	EfMapDocID    int    `json:"ef_map_docid"`
}
