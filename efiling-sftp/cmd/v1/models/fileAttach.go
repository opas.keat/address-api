package models

type FileAttach struct {
	Version        int    `json:"version"`        //1
	Fileattachname string `json:"fileAttachName"` //from json file_name
	Fileattachtype string `json:"fileAttachType"` // .pdf ต้องมี .
	Linktype       string `json:"linkType"`       //"dms"
	Linkid         int    `json:"linkId"`         //docId
	Filebase64     string `json:"fileBase64"`
	Referenceid    int    `json:"referenceId"` //0
	Secrets        int    `json:"secrets"`     //1
	Order          int    `json:"order"`       //0
	Type           int    `json:"type"`        //from json doc_type_id
	Folderid       int    `json:"folderId"`    //from dms api
	Systemid       int    `json:"systemId"`    //from json system_id
	Refno          string `json:"refNo"`       //from json ref_no
	Approve        int    `json:"approve"`
	OwnerType      string `json:"ownerType"`
	OwnerName      string `json:"ownerName"`
	InputFileType  string `json:"inputFileType"`
	AttachDate     string `json:"attachDate"`
}

type FileAttachResponse struct {
	Data         FileAttachResponseData `json:"data"`
	Success      bool                   `json:"success"`
	ErrorMessage string                 `json:"errorMessage"`
	Message      string                 `json:"message"`
}

type FileAttachResponseData struct {
	Version        int    `json:"version"`
	ID             int    `json:"id"`
	FileAttachName string `json:"fileAttachName"`
	FileAttachType string `json:"fileAttachType"`
	FileAttachSize int    `json:"fileAttachSize"`
	LinkType       string `json:"linkType"`
	LinkID         int    `json:"linkId"`
	URL            string `json:"url"`
	ThumbnailURL   string `json:"thumbnailUrl"`
	URLNoName      string `json:"urlNoName"`
	ReferenceID    int    `json:"referenceId"`
	Secrets        int    `json:"secrets"`
	Type           int    `json:"type"`
	PublicAttach   int    `json:"publicAttach"`
	Auth           int    `json:"auth"`
	Order          int    `json:"order"`
	Approve        int    `json:"approve"`
	SystemID       int    `json:"systemId"`
	RefNo          string `json:"refNo"`
	OwnerType      string `json:"ownerType"`
	OwnerName      string `json:"ownerName"`
	InputFileType  string `json:"inputFileType"`
	AttachDate     string `json:"attachDate"`
}
