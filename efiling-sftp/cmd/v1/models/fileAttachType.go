package models

type FileAttachType struct {
	Data    FileAttachTypeData `json:"data"`
	Success bool               `json:"success"`
	Message string             `json:"message"`
}

type FileAttachTypeData struct {
	ID                 int    `json:"id"`
	FileAttachTypeName string `json:"fileAttachTypeName"`
	FileAttachTypeCode string `json:"fileAttachTypeCode"`
}
