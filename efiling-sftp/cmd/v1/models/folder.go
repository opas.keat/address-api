package models

type FolderResponse struct {
	Data []struct {
		ID                           int         `json:"id"`
		Foldertype                   string      `json:"folderType"`
		Folderparentid               int         `json:"folderParentId"`
		Folderparenttype             string      `json:"folderParentType"`
		Folderparentkey              string      `json:"folderParentKey"`
		Folderorderid                float64     `json:"folderOrderId"`
		Foldernodelevel              int         `json:"folderNodeLevel"`
		Createdate                   string      `json:"createDate"`
		Removedate                   string      `json:"removeDate"`
		Createby                     int         `json:"createBy"`
		Removeby                     int         `json:"removeBy"`
		Updatedate                   string      `json:"upDateDate"`
		Updateby                     int         `json:"upDateBy"`
		Folderdescription            string      `json:"folderDescription"`
		Foldername                   string      `json:"folderName"`
		Foldertypeexpire             string      `json:"folderTypeExpire"`
		Foldertypeexpirenumber       int         `json:"folderTypeExpireNumber"`
		Documenttypeid               int         `json:"documentTypeId"`
		Iconcolor                    interface{} `json:"iconColor"`
		Icon                         interface{} `json:"icon"`
		Userprofilecreate            interface{} `json:"userProfileCreate"`
		Userprofileupdate            interface{} `json:"userProfileUpdate"`
		Dmsfoldertypepreexpire       interface{} `json:"dmsFolderTypePreExpire"`
		Dmsfoldertypepreexpirenumber int         `json:"dmsFolderTypePreExpireNumber"`
		Dmsemailuserpreexpire        string      `json:"dmsEmailUserPreExpire"`
		Dmsuserpreexpire             int         `json:"dmsUserPreExpire"`
		Dmsuserprofilepreexpire      interface{} `json:"dmsUserProfilePreExpire"`
		Dmssearchid                  interface{} `json:"dmsSearchId"`
		Fullpathname                 interface{} `json:"fullPathName"`
		Watermark                    int         `json:"watermark"`
		Contractcontroldocid1        int         `json:"contractControlDocId1"`
		Contractcontroldocid2        int         `json:"contractControlDocId2"`
		Contractcontroldocid3        int         `json:"contractControlDocId3"`
		Contractcontroldocid4        int         `json:"contractControlDocId4"`
		Capacity                     int         `json:"capacity"`
		Capacityuse                  int         `json:"capacityUse"`
		Sec                          int         `json:"SEC"`
		Dmsfoldercode                string      `json:"dmsFolderCode"`
	} `json:"data"`
	Success      bool   `json:"success"`
	Errormessage string `json:"errorMessage"`
	Message      string `json:"message"`
}
