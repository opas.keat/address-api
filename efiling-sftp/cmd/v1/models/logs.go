package models

import "gorm.io/gorm"

type Logs struct {
	gorm.Model `json:"-"`
	ID         uint   `json:"id"`
	CreatedBy  string `json:"-"`
	Ip         string `json:"ip"`
	UseApi     string `json:"useApi"`
}
