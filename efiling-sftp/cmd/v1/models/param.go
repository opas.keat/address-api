package models

type ParamResponse struct {
	Success bool   `json:"success"`
	Message string `json:"message"`
	Data    Param  `json:"data"`
}
type Param struct {
	ID    string `json:"id"`
	Name  string `json:"paramName"`
	Value string `json:"paramValue"`
}
