package models

type UserProfile struct {
	Data struct {
		ID        int     `json:"id"`
		OrderNo   float64 `json:"orderNo"`
		Structure struct {
			ID            int         `json:"id"`
			OrderNo       float64     `json:"orderNo"`
			Name          string      `json:"name"`
			ShortName     interface{} `json:"shortName"`
			Detail        string      `json:"detail"`
			Code          string      `json:"code"`
			NodeLevel     int         `json:"nodeLevel"`
			ParentID      int         `json:"parentId"`
			ParentKey     string      `json:"parentKey"`
			StructureTree string      `json:"structureTree"`
		} `json:"structure"`
		Title struct {
			ID          int         `json:"id"`
			Name        string      `json:"name"`
			NameEng     string      `json:"nameEng"`
			CreatedDate string      `json:"createdDate"`
			Type        interface{} `json:"type"`
		} `json:"title"`
		FirstName       string `json:"firstName"`
		LastName        string `json:"lastName"`
		FullName        string `json:"fullName"`
		Email           string `json:"email"`
		UserProfileType struct {
			ID   int    `json:"id"`
			Name string `json:"name"`
		} `json:"userProfileType"`
		Tel           string      `json:"tel"`
		FirstNameEng  string      `json:"firstNameEng"`
		LastNameEng   string      `json:"lastNameEng"`
		FullNameEng   string      `json:"fullNameEng"`
		Code          string      `json:"code"`
		Address       string      `json:"address"`
		Position      interface{} `json:"position"`
		PositionType  interface{} `json:"positionType"`
		PositionLevel int         `json:"positionLevel"`
		User          struct {
			ID   int    `json:"id"`
			Name string `json:"name"`
		} `json:"user"`
		UserStatus struct {
			ID   int    `json:"id"`
			Name string `json:"name"`
		} `json:"userStatus"`
		CojEmpCode string `json:"cojEmpCode"`
	} `json:"data"`
	Success      bool   `json:"success"`
	ErrorMessage string `json:"errorMessage"`
	Message      string `json:"message"`
}
