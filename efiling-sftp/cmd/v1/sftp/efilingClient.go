package sftp

import (
	"bufio"
	"bytes"
	"efilingsftp/cmd/v1/models"
	"efilingsftp/cmd/v1/util"
	"encoding/base64"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"golang.org/x/crypto/ssh"

	"github.com/pkg/sftp"
)

var ParamConfig *models.Param
var DmsApiServer *string

var USER = flag.String("user", "ecms", "ssh user")
var PASSWD = flag.String("pass", "P@ssw0rd", "ssh passwd")
var HOST = flag.String("host", "10.100.78.25", "ssh server ip")
var PORT = flag.Int("port", 22, "ssh port")
var RPATH = flag.String("remotepath", "/efiling/in/0000011/20210402/efilling_001_example/efilling_001_example.json", "remote path")
var API_KEY = "cios"

func SftpCreate(user string, passwd string, host string, port int) (*sftp.Client, error) {
	auth := make([]ssh.AuthMethod, 0)
	auth = append(auth, ssh.Password(passwd))
	config := ssh.ClientConfig{
		User:            user,
		Auth:            auth,
		Timeout:         10 * time.Second,
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}

	addr := fmt.Sprintf("%s:%d", host, port)
	conn, err := ssh.Dial("tcp", addr, &config)
	if err != nil {
		return nil, err
	}

	fmt.Println("connet sftp server ok.")
	c, err := sftp.NewClient(conn)
	if err != nil {
		conn.Close()
		return nil, err
	}
	fmt.Println("create sftp client ok.")
	return c, nil
}

func listFiles(sc *sftp.Client, remoteDir string, saveDir string) (err error) {
	fmt.Fprintf(os.Stdout, "Listing [%s] ...\n\n", remoteDir)

	files, err := sc.ReadDir(remoteDir)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to list remote dir: %v\n", err)
		return
	}

	for _, f := range files {
		var name, modTime, size string

		name = f.Name()
		modTime = f.ModTime().Format("2006-01-02 15:04:05")
		size = fmt.Sprintf("%12d", f.Size())

		if f.IsDir() {
			name = name + "/"
			modTime = ""
			size = "PRE"
		}
		// Output each file name and size in bytes
		fmt.Fprintf(os.Stdout, "%19s %12s %s\n", modTime, size, name)
		err := os.MkdirAll(saveDir+name, os.ModePerm)
		if err != nil {
			log.Println(err)
		}
		downloadFile(sc, remoteDir+name, saveDir+name)

	}

	return
}

func downloadFile(sc *sftp.Client, remoteFile, localFile string) (err error) {

	files, err2 := sc.ReadDir(remoteFile)
	if err2 != nil {
		fmt.Fprintf(os.Stderr, "Unable to list remote dir 2 : %v\n", err2)
		return
	}

	for _, f := range files {
		var name, modTime, size string

		name = f.Name()
		modTime = f.ModTime().Format("2006-01-02 15:04:05")
		size = fmt.Sprintf("%12d", f.Size())

		if f.IsDir() {
			name = name + "/"
			modTime = ""
			size = "PRE"
		}
		// Output each file name and size in bytes
		fmt.Fprintf(os.Stdout, "%19s %12s %s\n", modTime, size, name)

		fmt.Fprintf(os.Stdout, "Downloading [%s] to [%s] ...\n", remoteFile+name, localFile+name)
		// Note: SFTP To Go doesn't support O_RDWR mode
		srcFile, err := sc.OpenFile(remoteFile+name, (os.O_RDONLY))
		if err != nil {
			fmt.Fprintf(os.Stderr, "Unable to open remote file 3: %v\n", err)

		}
		defer srcFile.Close()

		dstFile, err := os.Create(localFile + name)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Unable to open local file 4 : %v\n", err)

		}
		defer dstFile.Close()

		bytes, err := io.Copy(dstFile, srcFile)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Unable to download remote file 5: %v\n", err)
			os.Exit(1)
		}
		fmt.Fprintf(os.Stdout, "%d bytes copied\n", bytes)
	}

	return
}

func ConectEFiling(remotepath string, saveDir string) {
	cojId := ParamConfig.Value
	log.Println(cojId)
	user := "ecms"
	passwd := "P@ssw0rd"
	host := "10.100.78.25"
	port := 22
	// currentTime := time.Now()
	// folderDate := currentTime.Format("20060102")
	// // fmt.Println(folderDate)

	// saveDir := "./" + folderDate + "/"
	// err := os.Mkdir(saveDir, 0755)
	// if err != nil {
	// 	log.Fatalln(err)
	// }
	// remotepath := "/efiling/in/" + cojId + "/" + folderDate + "/efilling_001_example/"
	// remotepath := "/efiling/in/" + cojId + "/20210402/efilling_001_example/"

	sclient, err := SftpCreate(user, passwd, host, port)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to start SFTP subsystem: %v\n", err)
		os.Exit(1)
	}
	defer sclient.Close()

	listFiles(sclient, remotepath, saveDir)
	fmt.Fprintf(os.Stdout, "\n")
}

func EFilingToStruct(filePath string) models.EFiling {
	// Open our jsonFile
	jsonFile, err := os.Open(filePath)
	// if we os.Open returns an error then handle it
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("Successfully Opened " + filePath)
	// defer the closing of our jsonFile so that we can parse it later on
	defer jsonFile.Close()

	// read our opened jsonFile as a byte array.
	byteValue, _ := ioutil.ReadAll(jsonFile)

	var efiling models.EFiling

	// we unmarshal our byteArray which contains our
	// jsonFile's content into 'users' which we defined above
	json.Unmarshal(byteValue, &efiling)

	return efiling
}

func GetEFiling(DmsApiServer string, sftpPath string, tempPath string, unzippath string, encrypt string) {

	cojId := GetCojIdFromParam(DmsApiServer)
	// cojId = "0000011"
	fmt.Println(cojId)
	nowDate := util.NowDate()
	fmt.Println(nowDate)
	if cojId == "" {
		log.Fatalln("cojId = " + cojId)
	}
	sftpPath = sftpPath + "/" + cojId + "/" + nowDate + "/"
	fmt.Println("sftpPath = " + sftpPath)
	// err := os.MkdirAll(sftpPath, os.ModePerm)
	// if err != nil {
	// 	log.Fatalln(err)
	// }
	tempPath = tempPath + "/" + nowDate + "/"
	fmt.Println("tempPath = " + tempPath)
	err := os.MkdirAll(tempPath, os.ModePerm)
	if err != nil {
		fmt.Println(err)
	}
	os.MkdirAll(unzippath, os.ModePerm)
	// /efilling/in/0000011/20210426/efilling_001_example
	// /efiling/in/3070107/20210610
	ConectEFiling(sftpPath, tempPath)
	fmt.Println("Load Files From FTP Finish...")

	// haveSftpFile := true
	var eFiling models.EFiling
	files, err := ioutil.ReadDir(tempPath)
	if err != nil {
		fmt.Println(err)
		// haveSftpFile = false
	}
	// if haveSftpFile {

	for _, file := range files {
		if file.IsDir() {
			fmt.Println("Path Data : " + tempPath + "/" + file.Name())
			tempPath2 := tempPath + "/" + file.Name()
			files2, err2 := ioutil.ReadDir(tempPath + "/" + file.Name())
			if err2 != nil {
				log.Println(err)
			}
			var fileName string
			var dmsFolderId int
			var documentId int
			for _, file2 := range files2 {
				fmt.Println("File Data : " + file2.Name())
				if strings.Contains(file2.Name(), ".json") {
					fileName = file2.Name()[:strings.IndexByte(file2.Name(), '.')]
					fmt.Println(fileName)
					eFiling = EFilingToStruct(tempPath2 + "/" + fileName + ".json")
					fmt.Println(eFiling)
					fmt.Println(eFiling.FolderID)
					dmsFolderId = GetFolderIdFromFolderIdJson(DmsApiServer, eFiling.FolderID)
					// dmsFolderId = 12
					fmt.Println(dmsFolderId)
					documentId = CheckDuplicateDoc(DmsApiServer, eFiling, dmsFolderId)
					// fmt.Println("document id = ")
					fmt.Println(documentId)
					if documentId == 0 {
						documentId = SaveDucument(DmsApiServer, eFiling, dmsFolderId)
					}
					fmt.Println(documentId)
				}
				if documentId != 0 {
					if strings.Contains(file2.Name(), fileName+".zip") {
						fmt.Println(encrypt + eFiling.RefNo)
						util.Unzip(encrypt+eFiling.RefNo, tempPath2+"/"+fileName+".zip", unzippath)

						UploadFileAttachToDms(DmsApiServer, eFiling, unzippath, documentId, dmsFolderId)
					}
				}

			}
		}

	}
	os.RemoveAll(tempPath)
	os.RemoveAll(unzippath)
	// }
}

func UploadFileAttachToDms(DmsApiServer string, eFiling models.EFiling, unzipPath string, documentId int, folderId int) {
	fmt.Println("Saving FileAttach API...")
	// responseId := []int{}
	for i, fl := range eFiling.FileList {
		fmt.Println(i, fl.RefNo)

		// base64.StdEncoding: Standard encoding with padding
		// It requires a byte slice so we cast the string to []byte
		attFile, err := os.Open(unzipPath + "/" + fl.FileName + "." + fl.FileType) // a QR code image
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		defer attFile.Close()
		reader := bufio.NewReader(attFile)
		content, _ := ioutil.ReadAll(reader)
		encodedStr := base64.StdEncoding.EncodeToString([]byte(content))
		// fmt.Println("Encoded:", encodedStr)

		fileAttachType := GetFileAttachType(DmsApiServer, fl.DocTypeID)
		userProfileId := GetUserProfileIdByFullName(DmsApiServer, fl.ApproveName)
		if userProfileId == 0 {
			userProfileId = 100000
		}
		// fmt.Println("Calling File Attach Type API...")
		// response, err := http.Get(DmsApiServer + "/pxapi/api/v1/FileAttachType/" + strconv.Itoa(fl.DocTypeID) + "?version=1&api_key=praXis")
		// if err != nil {
		// 	fmt.Printf("The HTTP request failed with error %s\n", err)
		// }
		// defer response.Body.Close()
		// bodyBytes, err := ioutil.ReadAll(response.Body)
		// if err != nil {
		// 	fmt.Print(err.Error())
		// }
		// var fileAttachType models.FileAttachType
		// json.Unmarshal(bodyBytes, &fileAttachType)
		// fmt.Printf("API File Attach Type as struct %+v\n", fileAttachType)
		// fmt.Printf("API Param as struct %+v\n", responseObject.Data)
		faType := fileAttachType
		apDate := util.ConvertDateToThai(fl.AttachDate, "-")
		fileAttach := models.FileAttach{
			Version:        1,
			Fileattachname: fl.FileName + "." + fl.FileType,
			Fileattachtype: fl.FileType,
			Linktype:       "dms",
			Linkid:         documentId,
			Filebase64:     encodedStr,
			Referenceid:    0,
			Secrets:        1,
			Order:          fl.Issue,
			Type:           faType,
			Folderid:       folderId,
			Systemid:       eFiling.SystemID,
			Refno:          fl.RefNo,
			Approve:        userProfileId,
			OwnerType:      fl.OwnerType,
			OwnerName:      fl.OwnerName,
			InputFileType:  fl.InputFileType,
			AttachDate:     apDate,
		}
		json_data, err := json.Marshal(fileAttach)
		if err != nil {
			fmt.Println(err)
			// return responseId
		}
		// fmt.Println(string(json_data))
		response2, err2 := http.Post(DmsApiServer+"/pxapi/api/v1/fileAttachs/base64?version=1&api_key="+API_KEY, "application/json", bytes.NewBuffer(json_data))
		if err2 != nil {
			fmt.Printf("The HTTP request failed with error %s\n", err2)
		}
		defer response2.Body.Close()
		bodyBytes2, err2 := ioutil.ReadAll(response2.Body)
		if err2 != nil {
			fmt.Print(err2.Error())
		}
		var responseObject models.FileAttachResponse
		json.Unmarshal(bodyBytes2, &responseObject)
		fmt.Printf("Saving File Attach Response as struct %+v\n", responseObject)
		// responseId[i] = responseObject.ID
	}
}

func SaveDucument(DmsApiServer string, eFiling models.EFiling, folderId int) int {
	fmt.Println("Saving Document API...")
	documentSave := models.DocumentSave{
		AcceptDate: "01/01/2564 00:00:00",
		BlackNo:    eFiling.BlackNo,
		Charge:     eFiling.Charge,
		FolderId:   folderId,
		Plaintiff:  eFiling.Plaintiff,
		Defendant:  eFiling.Defendant,
		CaseType:   eFiling.CaseType,
		RedNo:      eFiling.RedNo,
	}
	json_data, err := json.Marshal(documentSave)
	if err != nil {
		fmt.Println(err)
		return 0
	}
	fmt.Println(string(json_data))

	response, err := http.Post(DmsApiServer+"/pxapi/api/v1/dmsDocuments?api_key="+API_KEY, "application/json", bytes.NewBuffer(json_data))
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
	}
	defer response.Body.Close()
	bodyBytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Print(err.Error())
	}
	var responseObject models.DocumentResponse
	json.Unmarshal(bodyBytes, &responseObject)
	// fmt.Printf("Saving Document DocumentResponse as struct %+v\n", responseObject)
	return responseObject.Data.ID
}

func GetFolderIdFromFolderIdJson(DmsApiServer string, folderIdJson string) int {
	fmt.Println("Calling Dms API..." + folderIdJson)
	response, err := http.Get(DmsApiServer + "/pxapi/api/v1/dmsFolder/code/" + folderIdJson + "?version=1&api_key=praXis")
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
	}
	defer response.Body.Close()
	bodyBytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Print(err.Error())
	}
	var responseObject models.FolderResponse
	json.Unmarshal(bodyBytes, &responseObject)
	// fmt.Printf("API Dms as struct %+v\n", responseObject)
	return responseObject.Data[0].ID
}

func GetCojIdFromParam(DmsApiServer string) string {
	fmt.Println("Calling Param API...")
	response, err := http.Get(DmsApiServer + "/pxapi/api/v1/params/name/cojid?version=1&api_key=praXis")
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
	}
	defer response.Body.Close()
	bodyBytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Print(err.Error())
	}
	var responseObject models.ParamResponse
	json.Unmarshal(bodyBytes, &responseObject)
	fmt.Printf("API Param as struct %+v\n", responseObject)
	// fmt.Printf("API Param as struct %+v\n", responseObject.Data)
	ParamConfig = &responseObject.Data
	return ParamConfig.Value
}

func GetFileAttachType(DmsApiServer string, fileAttachTypeCode int) int {
	fmt.Println("Calling File Attach Type API...")
	response, err := http.Get(DmsApiServer + "/pxapi/api/v1/FileAttachType/code?code=" + strconv.Itoa(fileAttachTypeCode) + "&version=1&api_key=praXis")
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
	}
	defer response.Body.Close()
	bodyBytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Print(err.Error())
	}
	var responseObject models.FileAttachType
	json.Unmarshal(bodyBytes, &responseObject)
	fmt.Printf("API File Attach Type as struct %+v\n", responseObject)
	// fmt.Printf("API Param as struct %+v\n", responseObject.Data)
	return responseObject.Data.ID
}

func GetUserProfileIdByFullName(DmsApiServer string, fullName string) int {
	fmt.Println("Calling Get User Profile Id By FullName API...")
	response, err := http.Get(DmsApiServer + "/pxapi/api/v1/userProfiles/getByFullName/" + fullName + "?version=1&api_key=praXis")
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
	}
	defer response.Body.Close()
	bodyBytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Print(err.Error())
	}
	var responseObject models.UserProfile
	json.Unmarshal(bodyBytes, &responseObject)
	fmt.Printf("API Get User Profile Id By FullName as struct %+v\n", responseObject)
	// fmt.Printf("API Param as struct %+v\n", responseObject.Data)
	println("id=" + strconv.Itoa(responseObject.Data.ID))
	return responseObject.Data.ID
}

func CheckDuplicateDoc(DmsApiServer string, eFiling models.EFiling, folderId int) int {
	fmt.Println("Calling Check Duplicate Document API...")
	documentSave := models.DocumentSave{
		AcceptDate: "01/01/2564 00:00:00",
		BlackNo:    eFiling.BlackNo,
		Charge:     eFiling.Charge,
		FolderId:   folderId,
		Plaintiff:  eFiling.Plaintiff,
		Defendant:  eFiling.Defendant,
		CaseType:   eFiling.CaseType,
		RedNo:      eFiling.RedNo,
	}
	json_data, err := json.Marshal(documentSave)
	if err != nil {
		fmt.Println(err)
		return 0
	}
	fmt.Println(string(json_data))
	response, err := http.Post(DmsApiServer+"/pxapi/api/v1/dmsDocuments/duplicateDoc?api_key=praXis", "application/json", bytes.NewBuffer(json_data))
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
	}
	defer response.Body.Close()
	bodyBytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Print(err.Error())
	}
	type ResponseData struct {
		Data         int    `json:"data"`
		Success      bool   `json:"success"`
		ErrorMessage string `json:"errorMessage"`
		Message      string `json:"message"`
	}
	var responseObject ResponseData
	json.Unmarshal(bodyBytes, &responseObject)
	fmt.Printf("Saving CheckDuplicateDoc DocumentResponse as struct %+v\n", responseObject)
	return responseObject.Data
}
