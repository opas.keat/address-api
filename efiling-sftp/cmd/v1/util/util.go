package util

import (
	"bytes"
	"crypto/sha512"
	"encoding/hex"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/yeka/zip"
)

/*
สูตรการ encrypt / decrypt ไฟล์ สำหรับการเข้ารหัส file zip
eFiling3@COJ_<<ref_no>> (refno เป็นเลขที่อ้างอิงคำฟ้อง / คำร้อง เช่น 132021032300001 ) --> ไป Hash function SHA-512 --> แล้วนำ 12 ตัวสุดท้ายเป็น password เปิด zipfile
อ้างอิงได้จาก
https://emn178.github.io/online-tools/sha512.html
*/
func Unzip(input string, filePath string, upzipPath string) {
	sha_512 := sha512.New()
	sha_512.Write([]byte(input))
	// fmt.Printf("sha512:\t\t%x\n", sha_512.Sum(nil))
	secretSha512 := hex.EncodeToString(sha_512.Sum(nil))
	// fmt.Println(secretSha512)
	secret := secretSha512[len(secretSha512)-12:]
	// fmt.Println(secret)
	r, err := zip.OpenReader(filePath)
	if err != nil {
		log.Fatal(err)
	}
	defer r.Close()

	for _, f := range r.File {
		if f.IsEncrypted() {
			f.SetPassword(secret)
		}

		r, err := f.Open()
		if err != nil {
			log.Fatal(err)
		}

		buf, err := ioutil.ReadAll(r)
		if err != nil {
			log.Fatal(err)
		}
		ioutil.WriteFile(upzipPath+"/"+f.Name, buf, 0755)

		defer r.Close()
		// fmt.Println(f.Name)
		// fmt.Printf("Size of %v: %v byte(s)\n", f.Name, len(buf))
		// output, _ := iconv.ConvertString(f.Name, "utf-8", "windows-1252")
		// fmt.Println(toBinaryBytes(f.Name))
		// n := utf8.EncodeRune(buf, r)
		// if f.FileInfo().IsDir() {
		// 	// Make Folder
		// 	os.MkdirAll(fpath, os.ModePerm)
		// } else {
		// 	ioutil.WriteFile("./sentinel/validations/"+f.Name, buf ,os.ModePerm)
		// }

	}
}

func Zip(input string) {
	sha_512 := sha512.New()
	sha_512.Write([]byte(input))
	// fmt.Printf("sha512:\t\t%x\n", sha_512.Sum(nil))
	secretSha512 := hex.EncodeToString(sha_512.Sum(nil))
	fmt.Println(secretSha512)
	secret := secretSha512[len(secretSha512)-12:]
	fmt.Println(secret)

	contents, err := ioutil.ReadFile("./test_data/ใบแต่งทนายความ T1.pdf")
	fzip, err := os.Create("/Users/opas/Projects/go/gitlab.com/coj-link/test_data/20210409_1320210409058255.zip")
	if err != nil {
		log.Fatalln(err)
	}
	zipw := zip.NewWriter(fzip)
	defer zipw.Close()
	w, err := zipw.Encrypt("ใบแต่งทนายความ T1.pdf", secret, zip.StandardEncryption)
	if err != nil {
		log.Fatal(err)
	}
	_, err = io.Copy(w, bytes.NewReader(contents))
	if err != nil {
		log.Fatal(err)
	}
	zipw.Flush()
}

func toBinaryRunes(s string) string {
	var buffer bytes.Buffer
	for _, runeValue := range s {
		fmt.Fprintf(&buffer, "%b", runeValue)
	}
	return fmt.Sprintf("%s", buffer.Bytes())
}

func toBinaryBytes(s string) string {
	var buffer bytes.Buffer
	for i := 0; i < len(s); i++ {
		fmt.Fprintf(&buffer, "%b", s[i])
	}
	return fmt.Sprintf("%s", buffer.Bytes())
}

func ConvertDateToThai(dateIn string, separator string) string {
	s := strings.Split(dateIn, separator)
	yearI, err := strconv.Atoi(s[0])
	if err != nil {
		log.Fatal(err)
	}
	yearS := strconv.Itoa(yearI + 543)
	return s[2] + "/" + s[1] + "/" + yearS
}

func NowDate() string {
	currentTime := time.Now()
	return currentTime.Format("20060102")
}

// func DaysBetween(a, b time.Time) int {
// 	if a.After(b) {
// 		a, b = b, a
// 	}

// 	days := -a.YearDay()
// 	for year := a.Year(); year < b.Year(); year++ {
// 		days += time.Date(year, time.December, 31, 0, 0, 0, 0, time.UTC).YearDay()
// 	}
// 	days += b.YearDay()

// 	return days
// }

// func Date(s string) time.Time {
// 	d, _ := time.Parse("2006-01-02", s)
// 	return d
// }
