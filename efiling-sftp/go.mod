module efilingsftp

go 1.16

require (
	github.com/gofiber/fiber/v2 v2.12.0
	github.com/pkg/sftp v1.13.0
	github.com/spf13/viper v1.7.1
	github.com/yeka/zip v0.0.0-20180914125537-d046722c6feb
	golang.org/x/crypto v0.0.0-20210513164829-c07d793c2f9a
	gorm.io/driver/mysql v1.1.2
	gorm.io/gorm v1.21.12
)
